import { BaseObject } from "../core/base-object";
import { IGameContext } from "../core/game";
import { Vector } from "../core/vector";

export class Player extends BaseObject {
    position: Vector;
    width = 50
    height = 50

    speed = 40
    thrustForce = 0
    gravity = new Vector(0, 1)
    velocity = new Vector(0, 0)

    velocityWithGravity = new Vector(0, 0)

    constructor(gameContext: IGameContext, position: Vector) {
        super(gameContext, position)
    }

    public move(direction: Vector): void {
        this.position.add(direction.normalize().scale(this.speed))
    }

    public liftUp(): void {
        if (this.thrustForce < 2) {
            this.thrustForce += 0.1
        }
        this.velocity.add(new Vector(0, -1).scale(this.thrustForce))
    }

    public update(dt: number): void {
        if (this.gameContext.keyboardController.isKeyPressed('Space')) {
            this.liftUp()
        }
        
        this.position.add(this.velocity.plus(this.gravity).scale(this.gameContext.engine.totalTime))
    }

    public render(dt: number): void {
        this.ctx.fillRect(this.position.x, this.position.y, this.width, this.height)
        // this.position.render(this.ctx, )
    }
}