import { IGameContext } from "../core/game";
import { Vector } from "../core/vector";
import { Player } from "./player";

export class GameObjectManager {
    player: Player
    constructor(private gameContext: IGameContext) {
        this.player = new Player(gameContext, new Vector(this.gameContext.screen.WIDTH / 2, 20))
    }

    public update(dt: number): void {
        if (this.player.position.y + this.player.height > this.gameContext.screen.HEIGHT) {
            // stop player from falling
        }

        // if (this.gameContext.keyboardController.isKeyPressed('Space')) {
        //     this.player.liftUp()
        // } else {
        //     if (this.player.thrustForce >= 0) {
        //         this.player.thrustForce -= 0.1
        //     }
        // }
        // if (this.gameContext.keyboardController.isKeyPressed('KeyA')) {
        //     this.player.move(new Vector(1, -2))
        // }
        // if (this.gameContext.keyboardController.isKeyPressed('KeyD')) {
        //     this.player.move(new Vector(-1, -2))
        // }
        // console.log(this.player.position)
        
        this.player.update(dt)
    }

    public render(dt: number): void {
        this.gameContext.infoScreen.ctx.fillText('player position ' + this.player.position.toString(), 20, 20)
        this.gameContext.infoScreen.ctx.fillText('time elapsed ' + this.gameContext.engine.totalTime.toFixed(1).toString(), 20, 40)
        this.gameContext.infoScreen.ctx.fillText('thrust force ' + this.player.thrustForce.toString(), 20, 60)
        this.gameContext.infoScreen.ctx.fillText('gravity force ' + this.player.velocityWithGravity.toString(), 20, 80)
        this.player.render(dt)
        this.player.position.render(this.gameContext.screen.ctx, this.player.position)
    }
}