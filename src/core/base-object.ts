import { IGameContext } from "./game"
import { Vector } from "./vector"

export abstract class BaseObject {
    gameContext: IGameContext
    position: Vector

    constructor(gameContext: IGameContext, position: Vector) {
        this.gameContext = gameContext
        this.position = position
    }

    protected get ctx(): CanvasRenderingContext2D {
        return this.gameContext.screen.ctx
    }
}