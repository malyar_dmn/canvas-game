export class Vector {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }

    public add(vector: Vector): void {
        this.x += vector.x
        this.y += vector.y
    }

    public plus(vector: Vector): Vector {
        return new Vector(this.x + vector.x, this.y + vector.y)
    }

    public scale(value: number): Vector {
        return new Vector(this.x *= value, this.y *= value)
    }

    public normalize(): Vector {
        const x = this.x / this.length()
        const y = this.y / this.length()
        return new Vector(x, y)
    }

    public clone(): Vector {
        return new Vector(this.x, this.y)
    }

    public length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y)
    }

    public render(ctx: CanvasRenderingContext2D, position: Vector): void {
        ctx.beginPath()
        ctx.moveTo(position.x, position.y)
        ctx.lineTo(this.x, this.y)
        ctx.strokeStyle = 'red'
        ctx.stroke()
        ctx.closePath()
    }

    public toString(): string {
        return `[x: ${this.x}, y: ${this.y}]`
    }
}


