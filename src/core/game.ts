import { GameObjectManager } from "../objects/game-object-manager";
import { Engine } from "./engine";
import { KeyboardController } from "./keyboardController";
import { Screen } from "./screen";

export interface IGameContext {
    screen: Screen
    infoScreen: Screen
    keyboardController: KeyboardController
    engine: Engine
}

export class Game implements IGameContext {
    engine: Engine
    screen: Screen
    infoScreen: Screen
    keyboardController: KeyboardController

    gameObjectManager: GameObjectManager


    constructor() {
        this.engine = new Engine({
            update: this.update.bind(this),
            render: this.render.bind(this)
        })
        this.keyboardController = new KeyboardController()

        // screens
        this.screen = new Screen({ x: 0, y: 0 })
        this.infoScreen = new Screen({ x: 650, y: 0, width: 300 })

        this.gameObjectManager = new GameObjectManager(this)
    }

    public start() {
        this.engine.start()
    }

    update(dt: number): void {
        this.gameObjectManager.update(dt)
    }

    render(dt: number): void {
        this.screen.clear()
        this.infoScreen.clear()
        // render objects...
        this.gameObjectManager.render(dt)
        this.screen.drawBorder()
        this.infoScreen.drawBorder()

    }
}