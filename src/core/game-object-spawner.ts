import { IGameContext } from "./game";
import { Vector } from "./vector";

export abstract class GameObjectSpawner<T> {
    gameContext: IGameContext
    items: T[] = []
    constructor(gameContext: IGameContext) {
        this.gameContext = gameContext
    }

    public deleteItem(item: T): void {
        this.items = this.items.filter(el => el !== item)
    }

    public abstract spawn(position: Vector, ...args: any[]): void
}