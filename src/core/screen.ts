export interface IScreenOptions {
    x: number
    y: number
    width: number
    height: number
}

export class Screen {
    x: number
    y: number
    WIDTH: number
    HEIGHT: number
    canvasContext: CanvasRenderingContext2D

    constructor(screenOptions: Partial<IScreenOptions>) {
        this.x = screenOptions.x ?? 0
        this.y = screenOptions.y || 0
        this.WIDTH = screenOptions.width || 600
        this.HEIGHT = screenOptions.height || 600
        this.init()
    }

    init() {
        const canvas = document.createElement('canvas')
        canvas.width = this.WIDTH
        canvas.height = this.HEIGHT
        this.canvasContext = canvas.getContext('2d')

        canvas.style.position = 'absolute'
        canvas.style.top = `${this.y}px`
        canvas.style.left = `${this.x}px`

        document.body.style.position = 'relative'
        document.body.appendChild(canvas)

        document.body.style.margin = '0px'
        document.body.style.padding = '0px'
    }

    get ctx(): CanvasRenderingContext2D {
        return this.canvasContext
    }

    public drawBorder(): void {
        this.ctx.strokeStyle = 'black'
        this.ctx.strokeRect(0, 0, this.WIDTH, this.HEIGHT)
    }

    public clear(): void {
        this.ctx.clearRect(0, 0, this.WIDTH, this.HEIGHT)
    }
}