export interface IEngineListener {
    update(dt: number): void
    render(dt: number): void
}

export class Engine {
    step = 1 / 60
    dt = 0
    last = 0
    renderTime = 0
    totalTime = 0
    listener: IEngineListener

    constructor(listener: IEngineListener) {
        this.listener = listener
    }

    public start(): void {
        this.loop()
    }


    private loop() {
        const now = Date.now()
        this.dt = this.dt + Math.min(1, (now - this.last) / 1000)

        while (this.dt > this.step) {
            this.dt = this.dt - this.step
            this.totalTime += this.step
            this.listener.update(this.step)
        }


        this.last = now
        this.listener.render(this.dt)
        this.renderTime += this.dt

        window.requestAnimationFrame(this.loop.bind(this))
    }
}