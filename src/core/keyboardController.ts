export class KeyboardController {
    keySet = new Set()

    constructor() {
        window.addEventListener('keydown', this.onKeyDown.bind(this))
        window.addEventListener('keyup', this.onKeyUp.bind(this))
    }

    private onKeyDown(e: KeyboardEvent) {
        console.log('down', e.code);
        this.keySet.add(e.code)
    }

    private onKeyUp(e: KeyboardEvent) {
        this.keySet.delete(e.code)
    }

    public isKeyPressed(code: string) {
        return this.keySet.has(code)
    }

    public isKeyPressedOnce(code: string) {
        const isPressed = this.keySet.has(code)
        this.keySet.delete(code)
        return isPressed
    }
}